(function(){
    document.getElementById('sandbox').addEventListener("submit", function(e){
        e.preventDefault();
        // get the form entries
        let store = document.getElementById('store').value
        let api = document.getElementById('api').value
        let route = document.getElementById('route').value
        // setup the request
        let request = SO.baseURL('http://localhost/WSC/so-api/').storeId(store).key(api)
        // send the request
        request.get(route).then(function(res){
            document.getElementById('displayResults').innerHTML = JSON.stringify(res)
        }).catch(function() {
            document.getElementById('displayResults').innerHTML = "Something is not right here..."
        })
    })
})();
