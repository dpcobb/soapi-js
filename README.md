# soapi-js

JS Client library to help with testing of API.

## Usage

Include soapi.js file, a new instance of the library is defined as ```SO``` in the library. Add the following to an HTML file to test the Hello World route

```
<html>
<head>
    <script src="soapi.js"></script>
</head>
<body>
    <div id = "test"></div>
    <script>
        // Define the URL to access
        let url = "http://localhost/WSC/so-api/1.0.0/controller"
        // Set up the request
        let request = SO.storeId('STO-123').key('123456')
        // Send the request and get the response
        request.get(url).then(function(res){
            document.getElementById('test').innerHTML = res.body.text
        }).catch(function() {
            // An error occurred
        })
    </script>
</body>
</html>
```

## Methods

### Methods to Set Up the Request

#### storeId({ID}) - Required

Your store ID

#### key({API KEY}) - Required

The stores API Key

#### baseURL({BASE URL}) - Optional, but suggested

This is the base URL of the request, https://api.subscriptionsonly.com/. This allows the URL passed into the methods to be shorter and more easily debugged. The URL would then be '1.0.0/{END POINT}'. You do not need to concatenate these values, the SDK does that if baseURL exists.

### Adding Data to POST/PUT Requests

The following methods are available to add data to POST/PUT requests.

#### getForm({ID})

If using getForm your form inputs _must_ use attribute ```so-field``` with the appropriate option name, ex:

```
<form id="getForm">
    <label for="f_name">First Name</label>
    <input type="text" id="f_name" name="f_name" so-field="first_name"/>
    <label for="l_name">Last Name</label>
    <input type="text" id="l_name" name="l_name" so-field="last_name"/>
    <label for="email">Email</label>
    <input type="text" id="email" name="email" so-field="email"/>
    <input type="submit" value="Send Request"/>
</form>
```

Attach an event listener to the submit and build the data object:

```
document.getElementById('getForm').addEventListener("submit", function(e){
    e.preventDefault();
    let request = SO.baseURL('http://localhost/WSC/so-api/').storeId('STO-123').key('123456')
    request.getForm('getForm')
    request.post('1.0.0/xyz').then(function(res){
        // handle the response
    }).catch(function() {
        // handle errors
    })
})
```

When using getForm you pass the form ID name and the library finds the form and builds an object based on the ```so-field``` value and the input value.

#### formData({OBJECT})

If using formData an object must be given as a parameter.

Attach an event listener to the submit and build the data object:

```
document.getElementById('getForm').addEventListener("submit", function(e){
    e.preventDefault();
    let request = SO.baseURL('http://localhost/WSC/so-api/').storeId('STO-123').key('123456')
    request.formData({name:'daniel', email:'test@test.com'})
    request.post('1.0.0/xyz').then(function(res){
        // handle the response
    }).catch(function() {
        // handle errors
    })
})
```
