/*
SOAPI JS Client Library
DCobb 10/2018
Helps ensure JS requests are properly being sent to the API
 */
class SOAPI {
    constructor (soapi) {
        this.soapi = soapi
    }
    storeId(id){
        const api = Object.assign({}, this.soapi)
        api.storeId = id
        return new SOAPI(api)
    }
    key(key){
        const api = Object.assign({}, this.soapi)
        api.key = key
        return new SOAPI(api)
    }
    ver(ver){
        const api = Object.assign({}, this.soapi)
        api.ver = ver
        return new SOAPI(api)
    }
    baseURL(baseURL){
        const api = Object.assign({}, this.soapi)
        api.baseURL = baseURL
        return new SOAPI(api)
    }
    // formData sets the data if given an object
    formData(data){
        const api = Object.assign({}, this.soapi)
        api.data = JSON.stringify(data)
        return new SOAPI(api)
    }
    // getForm will build an object of values from a form with so-field attributes set
    getForm(formId){
        let form = document.getElementById(formId)
        let inputs = form.elements
        let item;
        let data = {}
        for(item in inputs){
            if(inputs[item].nodeName === "INPUT" && inputs[item].type != "submit"){
                data[inputs[item].getAttribute('so-field')] = inputs[item].value
            }
        }
        const api = Object.assign({}, this.soapi)
        api.data = JSON.stringify(data)
        return new SOAPI(api)
    }
    // Print data just used for testing, will be removed later
    printData(){
        let so = this.soapi
        console.log(so.data)
    }
    get(url){
        let so = this.soapi
        if(so.baseURL){
            url = so.baseURL + url
        }
        return new Promise (function(resolve, reject){
            let xhttp = new XMLHttpRequest(so);
            let token = 'Basic ' + btoa(so.storeId + ':' + so.key)
            let ver = so.ver
            xhttp.onload = function() {
                let res = JSON.parse(this.responseText)
                resolve(res);
            };
            xhttp.onerror = reject;
            xhttp.open("GET", url, true)
            xhttp.setRequestHeader("Authorization", token);
            xhttp.setRequestHeader("Accept", "application/json");
            xhttp.setRequestHeader("SO-Ver", ver);
            xhttp.send()
        })
    }

}
const SO = new SOAPI
